//
//  ViewController.swift
//  LudoDiceApp
//
//  Created by Manoj on 8/10/20.
//  Copyright © 2020 Manoj. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController,CLLocationManagerDelegate {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    var locationManager = CLLocationManager()
    @IBOutlet weak var addressUIL: UILabel!
    @IBOutlet weak var weatherUIL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Location Manager set up
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    /*
     ** Roll Random Dice Method
     */
    @IBAction func rollDiceTapped(_ sender: UIButton) {
        let firstNumber = arc4random_uniform(5) + 1
        let secondNumber = arc4random_uniform(5) + 1
        leftImageView.image = UIImage(named: "Dice\(firstNumber)")
        rightImageView.image = UIImage(named: "Dice\(secondNumber)")
    }
    /*
     ** Location Update Method
     */
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            if let error = error {
                print("Unable to Reverse Geocode Location (\(error))")
            } else {
                if let placemarks = placemarks, let placemark = placemarks.first {
                    var addressString : String = ""
                    if let localValue = placemark.subThoroughfare{
                        addressString = addressString + localValue + ", "
                    }
                    if let localValue = placemark.subLocality{
                        addressString = addressString + localValue + ", "
                    }
                    if let localValue = placemark.thoroughfare{
                        addressString = addressString + localValue + ", "
                    }
                    if let localValue = placemark.locality{
                        addressString = addressString + localValue + ", "
                    }
                    if let localValue = placemark.country{
                        addressString = addressString + localValue + ", "
                    }
                    if let localValue = placemark.postalCode{
                        addressString = addressString + localValue + ", "
                    }
                    self.addressUIL.text = addressString
                    print(addressString)
                    if let localValue = placemark.locality{
                        self.getResponseForURLWithParameters(url: String(format:"http://api.openweathermap.org/data/2.5/weather?q=%@&appid=2c2c5be9e6ca8695c7f90a9f9d1c3c96",localValue), userInfo: nil, type: "POST") { (responceData, responceStatus, error) in
                            if error == nil {
                                let jsonObject = try? JSONSerialization.jsonObject(with: responceData!, options: .allowFragments)
                                let jsonDict: NSDictionary? = jsonObject as? NSDictionary
                                if jsonDict != nil {
                                    let weather = jsonDict!["weather"] as! NSArray
                                    let weatherObject = weather[0] as! NSDictionary
                                    let weatherDetail = jsonDict!["main"] as! NSDictionary
                                    let windDetails = jsonDict!["wind"] as! NSDictionary
                                    var weatherString : String = ""
                                    if let localValue = weatherObject["main"] as? String{
                                        weatherString = weatherString + "main :" + localValue + ", "
                                    }
                                    if let localValue = weatherObject["description"] as? String{
                                        weatherString = weatherString + "description :" + localValue + ", "
                                    }
                                    if let localValue = weatherDetail["feels_like"] as? Double{
                                        weatherString = weatherString + "feels_like :" + String(localValue) + ", "
                                    }
                                    if let localValue = weatherDetail["humidity"] as? Int{
                                        weatherString = weatherString + "humidity :" + String(localValue) + ", "
                                    }
                                    if let localValue = weatherDetail["pressure"] as? Int{
                                        weatherString = weatherString + "pressure :" + String(localValue) + ", "
                                    }
                                    if let localValue = weatherDetail["temp"] as? Double{
                                        weatherString = weatherString + "temp :" + String(localValue) + ", "
                                    }
                                    if let localValue = weatherDetail["temp_max"] as? Double{
                                        weatherString = weatherString + "temp_max :" + String(localValue) + ", "
                                    }
                                    if let localValue = weatherDetail["temp_min"] as? Double{
                                       weatherString = weatherString + "temp_min :" + String(localValue) +  ", "
                                    }
                                    if let localValue = windDetails["speed"] as? Double{
                                        weatherString = weatherString + "wind_speed :" + String(localValue) + ", "
                                    }
                                   // print(weatherString)
                                    self.weatherUIL.text = weatherString
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /*
     ** Service call Method
     */
    func createURLRequestForURLWithType(urlString:String,type:String, timeoutIntervel: Int) -> NSMutableURLRequest {
           let url:NSURL! = NSURL.init(string: urlString as String)
           let urlRequest: NSMutableURLRequest! = NSMutableURLRequest.init(url: url as URL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: TimeInterval(timeoutIntervel))
           urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
           urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
           urlRequest.httpMethod = type as String
           return urlRequest
       }
       
       //MARK: Create URL Response
       func getResponseForURLWithParameters(url:String, userInfo:NSDictionary?, type:String, sessionTimeOut: Int = 300, completion:@escaping (Data?,HTTPURLResponse?,Error?)->()) {
           let urlRequest = createURLRequestForURLWithType(urlString: url as String,type: type as String, timeoutIntervel: sessionTimeOut)
           if userInfo != nil {
               let parametersData:Data = try! JSONSerialization.data(withJSONObject: userInfo!, options: .prettyPrinted)
               urlRequest.httpBody = parametersData as Data
             let string  = NSString.init(data: urlRequest.httpBody!, encoding:String.Encoding.utf8.rawValue)
             print("user parameters \(String(describing: string))  \n \(urlRequest)")
           }
           let urlSessionConfiguration:URLSessionConfiguration! = URLSessionConfiguration.default
           let urlSession:URLSession! = URLSession.init(configuration: urlSessionConfiguration, delegate: nil, delegateQueue: OperationQueue.main)
           let urlDataTask:URLSessionDataTask = urlSession.dataTask(with: urlRequest as URLRequest) { (data, urlResponse, error) in
               guard error == nil else{
                   completion(nil, nil, error)
                   return
               }
               guard let responceData = data else{
                   let userInfo: [NSObject : AnyObject] = [NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("Unauthorized", value: "did not receive data", comment: "") as AnyObject,NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("Unauthorized", value: "Account not activated", comment: "") as AnyObject
                   ]
                   let err = NSError(domain: "HttpResponseErrorDomain", code: 500, userInfo: (userInfo as! [String : Any]))
                   completion(data! as Data?,nil,err)
                   return
               }
               do {
                 let jsonResponce = try JSONSerialization.jsonObject(with: responceData, options: [])
                  print(jsonResponce)
                   let httpURLResponse:HTTPURLResponse = urlResponse as! HTTPURLResponse
                   completion(data! as Data?,httpURLResponse,nil)
               } catch {
                   completion(nil, nil, error)
                   print("error parsing response from POST on ")
                   return
               }
           }
           urlDataTask.resume()
       }
}

